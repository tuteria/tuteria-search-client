import decimal
import datetime
import json
import os
import requests
try:
    from urllib.parse import parse_qs, urlparse
except ImportError:
    from urlparse import parse_qs, urlparse
import pprint

class Helpers(object):

    def _map_key_to_obj(self, key):
        self.obj = {key: self.obj}
        return self

    def multi_match_query(self, fields, search_param, _type="cross_fields",
                          operator="and", **kwargs):
        if search_param:
            return dict(
                multi_match=dict(query=search_param, type=_type,
                                 operator=operator, fields=fields, **kwargs))

    def _dict_list(self, param, key_fields):
        """
        Returns a list of must dicts on a param
        : param param: the field to be checked
        : param key_fields: a list of keys to be populated as match dicts
        :return: a list of the results
        """
        if param:
            return [dict(match={key: param}) for key in key_fields]
        return []


class ComposeQuery(Helpers):

    def __init__(self):
        self.obj = {}

    def base_query(self, **kwargs):
        """root dictionary for search query"""
        self.obj = dict(query=self.obj, **kwargs)
        self.filter = {}
        return self

    def must_query(self, key, *args):
        self.obj = [self.filter_attr_query(key, x) for x in args]
        return self

    def filter_attr_query(self, key, value):
        """term,"""
        return {key: value}

    def match_query(self, *args):
        return self.must_query('match', *args)

    def bool_query(self, create=False, *kwargs):
        """must,should,must_not,filter"""
        value = ComposeQuery() if create else self
        params = kwargs if create else self.obj
        value.obj = dict(must=params)
        return value

    def filter_query(self, key, *kwargs):
        """gte, lte, exists, missing, term"""
        self.filter.update({key: kwargs})
        return self

    def nested_query(self, key):
        self.obj = dict(path=key, query=self.obj)
        return self

    def query_query(self):
        return self._map_key_to_obj('bool')

    def no_scoring_query(self):
        self.obj = dict(constant_score={"filter": self.obj})
        return self

    def final_query(self):
        return self._map_key_to_obj('query')

    def exact_search(self, key, value, bool=False):
        """
        {
          "query": { "bool": {
            "must": [
                {
                    "nested": { "path": "user", "query": {"bool": {
                        "must": [{"match": {"user.id": 4260}}]}}}
                }
              ]
        }}}
        """
        val = key.split('.')
        response = self.must_query('term', {key: value})\
            .bool_query().query_query().nested_query(val[0])
        if len(val) > 1:
            response = self.must_query('nested', response.obj)\
                .bool_query()
            if not bool:
                response = response.no_scoring_query()
            response = response.query_query()
        return response.base_query().obj

    def tutorskill_model_match(self, skill_name):
        """
        { "multi_match": {
            "query": "IELTS", "type": "cross_fields", "operator": "and",
            "fields": [ "skill", "heading" ], "tie_breaker": 0.3 } }
        """
        fields = ['skill', 'heading']
        return self.multi_match_query(fields, skill_name, tie_breaker=0.3,
                                      boost=10)

    def location_model_match(self, *args, **kwargs):
        """
        Returns a nested representation of a location field search along with
        ranking of result
        :params state: the state to be searched
        :params vicinity: the vicinity to be searched
        :params latitude: the latitude coordinate
        :params longitude: the longitude coordinate
        :return: a dictionary representing the query
        { "nested": { "path": "user.location", "query": { "function_score": {
            "query": { "bool": { "must": [ { "match": {"user.location.state":
            "Lagos" } } ], "should": [ { "match": { "user.location.vicinity":
            "Festac Town" } }, { "match": { "user.location.address":
            "Festac Town" } } ] } }, "functions": [ { "gauss": {
            "user.location.coordinate": { "origin": { "lon": "3.3766851",
            "lat": "6.5643530" }, "offset": "2km", "scale": "3km" } },
            "weight": 2 } ] } } }
        }
        """
        fields = ['state', 'vicinity', 'latitude', 'longitude']
        should_keys = ['user.location.vicinity', 'user.location.address']
        must_keys = ['user.location.state']
        state, vic, lat, lng = tuple(kwargs.get(i) for i in fields)
        must_search = self._dict_list(state, must_keys)
        # should_search = self._dict_list(vic, should_keys)
        should_search = [self.multi_match_query(
            should_keys, vic, _type="best_fields", operator="or", boost=40
        )]
        nested_query = ComposeQuery()
        nested_query.obj = dict(must=must_search, should=[o for o in should_search if o])
        functions = []
        if lat and lng:
            functions.append(self._guass_function(
                'user.location.coordinate', dict(lon=lat, lat=lng), "2km",
                "3km", 50))
        response = nested_query.query_query().base_query(functions=functions,
                                                         boost=50).function_score_query()\
            .nested_query('user.location').base_nested_query()

        return response.obj

    def function_score_query(self):
        return self._map_key_to_obj('function_score')

    def base_nested_query(self):
        return self._map_key_to_obj('nested')

    def _guass_function(self, key, origin, offset, scale, weight=1):
        return dict(gauss={
            key: dict(origin=origin, offset=offset, scale=scale),
        }, weight=weight)

    def profile_model_match(self, *args, **kwargs):
        """
        Returns a nested representation of the user's profile field
        :params gender: the gender to be searched
        :params curriculum: emphasis on the curriculum
        :params classes: the classes to be considered
        :params skill_name: the subject name to be searched
        :return: a dictionary representing the query

        { "nested": { "path": "user.profile", "query": { "bool": {
            "should": [ { "multi_match": { "query": "English", "fields": [
                "user.profile.description", "user.profile.tutor_description"
                ] } }, { "bool": { "should": [ { "multi_match": {
                "query": "british, primary, nursery", "fields": [
                "user.profile.curriculum", "user.profile.classes"] } } ] } } ],
            "must": [ { "match": { "user.profile.gender": "F" } } ] } } } },
        """
        base_str = 'user.profile'
        fields = ['gender', 'curriculum', 'classes', 'skill_name']
        must_keys = self._build_fields(base_str, ['gender'])
        should_keys = self._build_fields(base_str, ['curriculum', 'classes'])
        gender, curriculum, classes, skill_name = tuple(
            kwargs.get(i) for i in fields)
        must_search = self._dict_list(gender, must_keys)
        should_search = []
        if curriculum or classes:
            should_search.append(self.multi_match_query(
                should_keys,
                self._remove_none(curriculum, classes), _type="most_fields",
                operator="or"))
        inner_obj = ComposeQuery()
        outer_should = []
        if len(should_search) > 0:
            inner_obj.obj = dict(should=should_search)
            inner_obj = inner_obj.query_query()
            outer_should.append(inner_obj.obj)
        if skill_name:
            m_fields = self._build_fields(
                base_str, ['description', 'tutor_description'])
            mm = self.multi_match_query(m_fields, skill_name, operator="or")
            outer_should.append(mm)
        inner_obj.obj = dict(should=outer_should, must=must_search)
        response = inner_obj.query_query().nested_query(base_str)\
            .base_nested_query()
        return response.obj

    def user_model_match(self, *args, **kwargs):
        """
        Returns a nested representation of the user' field
        :params search_param: the param to be searched
        :return: a dictionary representing the query
        { "nested": { "path": "user", "query": { "function_score": { "query": {
            "bool": { "must": [ { "multi_match": { "query": "English",
                "fields": [ "user.educations", "user.work_experiences" ] } } ]
            } }, "functions": [ { "field_value_factor": {
            "field": "user.no_of_hours_taught", "modifier": "log1p" },
            "weight": 1.6 }, { "field_value_factor": {"field":
            "user.unique_rating_count", "modifier": "log1p" }, "weight": 1.1 }
            ] } } } }
        """
        base_str = 'user'
        fields = ['search_param']
        must_keys = self._build_fields(
            base_str, ['educations', 'work_experiences'])
        search_param, = tuple(kwargs.get(i) for i in fields)
        must_search = []
        if search_param:
            must_search.append(self.multi_match_query(
                must_keys, search_param, _type="best_fields", operator="or"))
        nested_query = ComposeQuery()
        nested_query.obj = dict(must=must_search)
        factors = [('no_of_hours_taught', 1.6), ('unique_rating_count', 1.1)]
        functions = [
            self._field_factor_function(
                x[0], weight=x[1], missing=0) for x in factors]
        response = nested_query.query_query().base_query(functions=functions)\
            .function_score_query()\
            .nested_query(base_str).base_nested_query()
        return response.obj

    def calendar_model_match(self, *args, **kwargs):
        """
        Returns a nested representation of the user calendar's field
        :params days: the weekdays to be searched
        :params times: the time_slots to be searched
        :return: a dictionary representing the query
          { "nested": { "path": "user.days", "query": { "bool": { "must": [
            { "match": { "user.days.weekday": "Monday" } }, { "match":{
            "user.days.time_slot": "Afternoon" } } ] } } } }
        """
        base_str = 'user.days'
        fields = ['days', 'times']
        must_keys = self._build_fields(
            base_str, ['weekday', 'time_slot'])
        days, times = tuple(kwargs.get(i) for i in fields)
        must_search = self._dict_list(days, [must_keys[0]])
        must_search.extend(self._dict_list(times, [must_keys[1]]))
        nested_query = ComposeQuery()
        nested_query.obj = dict(must=must_search)
        response = nested_query.query_query().nested_query(base_str)\
            .base_nested_query()
        return response.obj

    def ranking_function(self, *args, **kwargs):
        """
        [ { "random_score": { "seed": "the users session id" } }, {
        "gauss": { "price": { "origin": 1000, "offset": "200", "scale": "800"
        } }, "weight": 1.5 }, { "field_value_factor": { "field": "quiz_score",
        "missing": 0 }, "weight": 1.3 }, { "field_value_factor": {"field":
        "rating", "missing": 0 }, "weight": 1.2 }]
        """
        fields = ['session_id', 'price', ]
        session_id, price = (kwargs.get(i) for i in fields)
        factors = [('quiz_score', 1.3), ('rating', 1.2)]
        functions = [self._field_factor_function(
            x[0], weight=x[1], missing=0) for x in factors]
        if price:
            functions.append(self._guass_function(
                'price', price, "200", "800", 1.5))
        if session_id:
            functions.append(dict(random_score=dict(seed=session_id)))

        return functions

    def _field_factor_function(self, field, modifier="log1p", weight=1, **kwargs):
        inner_dict = dict(field=field, modifier=modifier, **kwargs)
        return dict(field_value_factor=inner_dict, weight=weight)

    def _build_fields(self, base, fields):
        return ["%s.%s" % (base, x) for x in fields]

    def _remove_none(self, *args):
        new_val = [x for x in args if x]
        return ", ".join(new_val)

    def _get_sort_order(self, sort_order):

    #   "user.unique_rating_count": {
    #     "nested_path": "user",
    #     "order": "desc"
    #   }
        print(sort_order)
        key_value = {
            'default': ('_score', 'desc'),
            'high_price': ('price', 'desc'),
            'low_price': ('price', 'asc'),
            'most_reviews': ('user.unique_rating_count', "desc"),
            'hours_taught': ('user.no_of_hours_taught', "desc")
        }
        option = key_value[sort_order]
        print(option)
        split = option[0].split(".")
        response = {
            option[0]: dict(order=option[1])
        }
        if len(split) > 1:
            response[option[0]].update(nested_path=split[0])
        return response

    def sorting_list(self, *args, **kwargs):
        base_str = "user.location"
        lat, lon = tuple(kwargs.get(x) for x in ['latitude', 'longitude'])
        arr = [self._get_sort_order(kwargs.get('sort_order', 'default')), ]
        if lat and lon:
            arr.append(dict(_geo_distance={
                'user.location.coordinate': dict(lat=lat, lon=lon),
                "order": "asc",
                "unit": "km",
                "distance_type": "plane",
                "nested_path": base_str
            }))
        return arr

    def search(self, *args, **kwargs):
        """
        {
          "query": {
            "function_score": {
              "query": {
                "bool": {
                  "must": [
                    self.tutorskill_model_match("IELTS"),
                    self.location_model_match(state='Lagos',
                        vicinity="Festac Town", latitude=2.33, longitude=3.32),
                    self.profile_model_match(
                        classes="primary,nursery", curriculum="british",
                        gender="F", skill_name="English Language"),
                    self.user_model_match(search_param="teacher")
                    self.calendar_model_match(days='Monday, Tuesday',
                        times='Morning')
                  ]
                }
              },
              "functions": self.ranking_function(price=1000),
              "score_mode": "avg"
            }
          }
        }
        """
        fields = ['skill_name', 'state', 'latitude', 'longitude',
                  'vicinity', 'classes', 'curriculum', 'gender', 'education',
                  'days', 'times']
        profile_kwargs = kwargs.copy()
        profile_kwargs.pop('skill_name')
        must_search = [
            self.tutorskill_model_match(kwargs.get('skill_name')),
            self.location_model_match(**kwargs),
            self.profile_model_match(**profile_kwargs),
            self.user_model_match(),
            self.calendar_model_match(**kwargs)
        ]
        self.obj = dict(must=must_search)
        query = self.query_query().base_query(
            functions=self.ranking_function(**kwargs), score_mode="avg")
        PAGE_SIZE = kwargs.get('size') or 10
        return query.function_score_query().base_query(
            sort=self.sorting_list(**kwargs)).obj


class TutorSkillIndex:
    _type = 'tutorskill'
    index = 'tuteria'
    BASE_URL = os.getenv('ELASTICSEARCH_URL', 'http://localhost:9200/')
    base_url = BASE_URL + index + "/"

    def __init__(self):
        self.query = ComposeQuery()

    def get_full_search_url(self,url='',size=10,_from=0, underscore=True):
        full_url = url
        if not full_url:
            full_url = self.base_url + self._type + "/_search?size=" + str(size)
        if _from:
            value = '&_from=' if underscore else '&from='
            full_url += value + str(_from)
        return full_url          
        
    def get_from_param(self, default_url='',**kwargs):
        if not default_url:
            return kwargs.get('_from') or 0
        params = parse_qs(urlparse(default_url).query)
        return params.get('_from')[0]

    def search(self, *args, **kwargs):
        """latitude,longitude,state,skill_name,gender,expectation,classes,
        curriculum,days,price,work_experiences"""
        size = kwargs.get('size') or 10
        size = int(size)
        url = kwargs.get('url')        
        _from = self.get_from_param(default_url=url, **kwargs)
        full_url = self.get_full_search_url(url=url, size=size, _from=_from, underscore=False)
        data = json.dumps(self.query.search(*args, **kwargs))
        response = requests.get(full_url, data=data )
        response.raise_for_status()
        result = response.json()
        final_result =  {
            'result': result['hits']['hits'],
            'total': result['hits']['total'],
        }
        total_page_size = final_result['total']
        if total_page_size > 0:
            _from = int(_from)
            current_page = int((int(_from) / size) + 1)
            total_pages = int(total_page_size / size)
            final_result.update({
                'current_page': current_page,
                'total_pages': total_pages,
            })
            if current_page < total_pages:
                y = int(size) + int(_from)
                final_result.update(
                    next_url= self.get_full_search_url(size=size,_from=y, underscore=True)
                )
        return final_result

    def get_tutorskill(self, id):
        response = requests.get(
            self.base_url + self._type + '/' + str(id)
        )
        response.raise_for_status()
        return response.json()['_source']

