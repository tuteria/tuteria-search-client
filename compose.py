
class Helpers(object):

    def _map_key_to_obj(self, key):
        self.obj = {key: self.obj}
        return self

    def multi_match_query(self, fields, search_param, _type="cross_fields",
                          operator="and", **kwargs):
        if search_param:
            return dict(
                multi_match=dict(query=search_param, type=_type,
                                 operator=operator, fields=fields, **kwargs))

    def _dict_list(self, param, key_fields):
        """
        Returns a list of must dicts on a param
        : param param: the field to be checked
        : param key_fields: a list of keys to be populated as match dicts
        :return: a list of the results
        """
        if param:
            return [dict(match={key: param}) for key in key_fields]
        return []


class ComposeQuery(Helpers):

    def __init__(self):
        self.obj = {}

    def base_query(self, **kwargs):
        """root dictionary for search query"""
        self.obj = dict(query=self.obj, **kwargs)
        self.filter = {}
        return self

    def must_query(self, key, *args):
        self.obj = [self.filter_attr_query(key, x) for x in args]
        return self

    def filter_attr_query(self, key, value):
        """term,"""
        return {key: value}

    def match_query(self, *args):
        return self.must_query('match', *args)

    def bool_query(self, create=False, *kwargs):
        """must,should,must_not,filter"""
        value = ComposeQuery() if create else self
        params = kwargs if create else self.obj
        value.obj = dict(must=params)
        return value

    def filter_query(self, key, *kwargs):
        """gte, lte, exists, missing, term"""
        self.filter.update({key: kwargs})
        return self

    def nested_query(self, key):
        self.obj = dict(path=key, query=self.obj)
        return self

    def query_query(self):
        return self._map_key_to_obj('bool')

    def no_scoring_query(self):
        self.obj = dict(constant_score={"filter": self.obj})
        return self

    def final_query(self):
        return self._map_key_to_obj('query')

    def exact_search(self, key, value, bool=False):
        """
        {
          "query": { "bool": {
            "must": [
                {
                    "nested": { "path": "user", "query": {"bool": {
                        "must": [{"match": {"user.id": 4260}}]}}}
                }
              ]
        }}}
        """
        val = key.split('.')
        response = self.must_query('term', {key: value})\
            .bool_query().query_query().nested_query(val[0])
        if len(val) > 1:
            response = self.must_query('nested', response.obj)\
                .bool_query()
            if not bool:
                response = response.no_scoring_query()
            response = response.query_query()
        return response.base_query().obj


 