import decimal
import datetime
import json
import os
import requests
from urllib.parse import parse_qs, urlparse
import pprint
from administration.models import TutorSkill, User
from .compose import ComposeQuery

V1 = False
def generate_mapping(fields):
    """Generate mapping for fields
    :param fields: a list of fields with either type string, dict or tuple
        in the mapping
    :return: a dictionary of the fields along with types
    """
    response = {}
    for i, field in enumerate(fields):
        if type(field) == dict:
            response[field['name']] = {"type": field['type']}
        elif type(field) == tuple:
            response[field[0]] = {"type": field[1]}
        else:
            response[field] = {"type": "string"}
    return response

user_fields = [
    "id", "slug", "email", "first_name", "last_name", "username",
    ("date_joined", "date"), "country", "educations", "work_experiences",
    ("no_of_hours_taught", "double"), ('rating', "double"),
    ('unique_rating_count', "long"), ('tuteria_points', "long"),
    "tutor_description","other_skills",
]
profile_fields = [
    "description", "gender", "video", "tutor_description",
    ("level", "long"), "teaching_years", "classes",
    "curriculum"
]
location_fields = [
    'address', 'state', 'vicinity', ('coordinate', "geo_point")
]
review_fields = [
    'modified', 'review', 'score', 'commenter_name'
]
days_fields = ['weekday', 'time_slot', 'last_modified']
user_mapping = generate_mapping(user_fields)
user_mapping.update({
    "profile": {
        "type": "nested",
        "properties":  generate_mapping(profile_fields)
    },
    "location": {
        "type": "nested",
        "properties": generate_mapping(location_fields)
    },
    "reviews": {
        "type": "nested",
        "properties": generate_mapping(review_fields)
    },
    "days": {
        "type": "nested",
        "properties": generate_mapping(days_fields)
    }
})
if V1:
    tutor_skill_fields = [
        "id", "slug", "skill", "heading", ("price", "double"), ("discount", "long"),
        "image","related_with", "exhibitions", "results_acheived", "teaching_style",
        "has_other_subjects", ("quiz_score", "long"), 'description'
    ]
else:
    tutor_skill_fields = [
        "id", "slug", "skill", "heading", ("price", "double"), ("discount", "long"),
        "image","related_with", "exhibitions", ("quiz_score", "long"), 'description'
    ]
tutor_skill_mapping = generate_mapping(tutor_skill_fields)
tutor_skill_mapping.update({
    'awards': {
        "type": "nested",
        "properties": generate_mapping(["award_name", "award_institution"])
    },
    "user": {
        "type": "nested",
        "properties": user_mapping
    }
})
mappings = {
    "mappings": {
        "tutorskill": {
            "properties": tutor_skill_mapping
        }
    }
}


class DecimalAndDateEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime("%Y-%m-%d")
        if isinstance(obj, decimal.Decimal):
            return str(obj)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


class TutorSkillIndex:
    _type = 'tutorskill'
    index = 'tuteria'
    BASE_URL = os.getenv('ELASTICSEARCH_URL', 'http://localhost:9200/')
    base_url = BASE_URL + index + "/"

    def __init__(self):
        self.query = ComposeQuery()

    def create_mapping(self):
        """Create mapping for the tutor_skills"""
        requests.delete(self.base_url)
        response = requests.put(
            self.base_url, data=json.dumps(mappings))
        print(response.text)

    def get_tutorskills(self, tutor_id=None):
        """get all active tutorskills along with related fields
        :param tutor_id: The id of the tutor whose skills are to be fetched
        :return: a queryset of all the tutorskills"""
        queryset = TutorSkill.query.filter(
            TutorSkill.status == TutorSkill.ACTIVE)
        if tutor_id:
            queryset = queryset.filter(TutorSkill.tutor_id == tutor_id)
        return queryset.all()

    def _populate_fields(self, field, value):
        """returns a dictionary of the field and value respectively"""
        new_field = field[0] if type(field) is tuple else field
        if value:
            return {new_field: getattr(value, new_field)}
        return {new_field: None}

    def _populate_list_fields(self, fields, value):
        """returns a list of serialized dictionaries
        :param fields: an array of the fields in the list to be serialized
        :param value: the list of values to be serialized
        :return: a list of dictionaries"""
        return [self._initialize_dictionary(fields, v) for v in value]

    def _initialize_dictionary(self, fields, value):
        """returns a dictionary of the fields as keys to properties in
        the value
        :param fields: a list of fields to be used as keys
        :param value: the object
        :return: a dictionary"""
        item = {}
        for field in fields:
            item.update(self._populate_fields(field, value))
        return item

    def _map_tutor_to_json(self, tutor):
        """Generates the string json representation of a tutor with
        respect to the tutorskill mapping
        :param tutor: The tutor to be decoded
        :return: a dictionary of the decoded result"""
        result = self._initialize_dictionary(user_fields, tutor)
        profile = self._initialize_dictionary(profile_fields, tutor.profile)
        location = self._initialize_dictionary(location_fields,
                                               tutor.tutor_location)
        reviews = self._populate_list_fields(review_fields, tutor.reviews)
        calendar = tutor.calendar_info
        if calendar:
            days = self._populate_list_fields(days_fields, calendar.days)
            result.update(days=days)

        result.update(profile=profile, location=location, reviews=reviews)
        return result

    def generate_bulk_insert_json_string(self, tutorskill):
        """Generates the string json for a bulk insert of a single tutor skill
        :param tutorskill: The tutor skill passed_field
        :return: a string of the decoded dictionary"""
        data = '{"index": {"_id": "%s"}}\n' % tutorskill.id
        temp_fields = tutor_skill_fields.copy()
        for field in ['image', 'skill']:
            temp_fields.remove(field)
        tutor_skill_dict = self._initialize_dictionary(
            temp_fields, tutorskill)
        tutor_skill_dict.update({
            "image": tutorskill.get_image(),
            "skill": tutorskill.skill.name,
            "user": self._map_tutor_to_json(tutorskill.tutor)
        })
        data += json.dumps(tutor_skill_dict, cls=DecimalAndDateEncoder) + '\n'
        return data

    def bulk_update_tutorskills(self, tutorskills):
        data = ''
        for ts in tutorskills:
            data += self.generate_bulk_insert_json_string(ts)
        response = requests.put(
            self.base_url + self._type + '/_bulk', data=data)
        if response.status_code < 400:
            print("Successfully saved")
        print(response.text)

    def populate_indices(self):
        self.bulk_update_tutorskills(self.get_tutorskills())

    def update_tutor_skills(self, tutor_id):
        """DELETE /tuteria/tutorskill/_query"""
        final_query = self.query.exact_search("user.id", tutor_id, bool=True)
        data = json.dumps(final_query)
        response = requests.delete(
            self.base_url + self._type + '/_query', data=data
        )
        if response.status_code < 400:
            self.bulk_update_tutorskills(self.get_tutorskills(tutor_id))
        response.raise_for_status()

    def update_viewed_skills(self, tutor_id):
        """increment the count for when profile is viewed and addr_type
        a timestamp for the last day the profile was viewed"""
        data = json.dumps({
            "script": {
                ('"inline": "ctx._source.counter = ctx._source.counter '
                 "? ctx._source.counter += 1 : 1; ctx._source.last_checked= "
                 "new Date();")
            }
        })
        response = requests.post(
            self.base_url + self._type + "/{}/_update".format(tutor_id),
            data=data
        )
        response.raise_for_status()





    def initialize(self):
        """Reset the mappings and repopulate the indexes of all tutorskills"""
        self.create_mapping()
        self.populate_indices()
