import flask
from . import search_api, TutorSkillIndex

@search_api.route('/the-apis')
def home():
    return flask.jsonify({'name':"Hello world"})

@search_api.route('/update-tutorskill/<int:tutor_id>')
def update_tutorskills(tutor_id):
	instance = TutorSkillIndex()
	instance.update_tutor_skills(tutor_id)
	return flask.jsonify({'updated':True})
