#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = 1.2

setup(
    name='Tuteria Search',
    version=str(version),
    author='',
    author_email='gbozee@gmail.com',
    packages=[
        'search_client',
    ],
    include_package_data=True,
    install_requires=[
        'requests',
    ],
    zip_safe=False,
)