from .elastic import TutorSkillIndex
from flask import Blueprint

search_api = Blueprint('search_api', __name__)

import search.views